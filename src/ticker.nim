import std/[asyncdispatch, times, sugar, asyncfutures]


proc main: int =
  proc ticker(ident, stop, interval: int) {.async.} =
    var remain = stop
    while remain > 0:
      await interval.sleepAsync
      echo ident, " ", now()
      dec remain

  let futs = collect:
    for ident in 1..3:
      ticker ident, 3, 1000

  # waitFor futs.foldl(a and b)
  waitFor all futs


when isMainModule:
  quit main()
