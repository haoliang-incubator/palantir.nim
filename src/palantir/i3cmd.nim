import std/[os, strutils]

import ./i3ipc/[sockpath, protocol]

when defined(asyncimpl):
  import std/asyncdispatch
  import ./i3ipc/asyncconnection
else:
  import ./i3ipc/connection

when defined(asyncimpl):
  proc roundtrip(path, msg: string): string =
    return waitFor asyncconnection.roundtrip(path, msg)

proc main: int =
  let
    rawcmd = commandLineParams().join(" ")
    i3cmd = mRunCommand.pack(rawcmd)
    path = findSockPath()

  echo roundtrip(path, i3cmd)
  echo "used-mem: ", getOccupiedMem()

when isMainModule:
  quit main()

# asyncrun: nim c -r --mm:orc
