import std/[strutils]

import struct

type
  MsgType* = enum
    mRunCommand,
    mGetWorkspaces,
    mSubscribe,
    mGetOutputs,
    mGetTree,
    mGetMarks,
    mGetBarConfig,
    mGetVersion,
    mGetBindingModes,
    mGetConfig,
    mSendTick,
    mSync,
    mGetBindingState

const magic* = "i3-ipc"
const headerSpec* = block:
  let l = magic.len + 4 * 2
  (len: l, high: l-1)

func pack*(t: MsgType, payload: string): string =
  result.add magic
  result.add struct.pack("@II", payload.len, t.ord)
  result.add payload

proc unpackHeader*(header: string): (MsgType, int) {.raises: [ValueError].} =
  if header.len != headerSpec.len:
    raise newException(ValueError, "invalid header due to length")

  if not header.startsWith(magic):
    raise newException(ValueError, "invalid header due to magic string")

  let
    r = struct.unpack("@II", header[magic.len .. headerSpec.high])
    rawLen = r[0].getUInt
    rawType = r[1].getUInt

  return (rawType.MsgType, rawLen.int)


proc unpackHeader*(header: seq[byte]): (MsgType, int) =
  unpackHeader(cast[string](header))
