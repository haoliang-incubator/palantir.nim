import std/[asyncdispatch, asyncnet, net, ropes, exitprocs]

import ./protocol

type AsyncConnection* = object
  sock: AsyncSocket

using self: ref AsyncConnection

proc init(self; path: string) {.async.} =
  let sock = newAsyncSocket(AF_UNIX, protocol = IPPROTO_IP)
  await sock.connectUnix path
  self.sock = sock

proc newAsyncConnection*(path: string): Future[ref AsyncConnection] {.async.} =
  result = new AsyncConnection
  asyncCheck result.init path

proc close*(self) =
  self.sock.close

proc sendmsg(self; msg: string) {.async.} =
  await self.sock.send msg

proc recvmsg(self): Future[string] {.async.} =
  let sock = self.sock

  let
    header = await sock.recv(headerSpec.len)
    (_, payloadSize) = header.unpackHeader()

  var
    payload: Rope
    remain = payloadSize

  while remain > 0:
    let chunk = await sock.recv(size = remain)
    assert chunk != "", "socket was broken"
    payload.add chunk
    remain -= chunk.len

  return $payload

proc roundtrip*(self; msg: string): Future[string] {.async.} =
  await self.sendmsg msg
  return await self.recvmsg

var conn: ref AsyncConnection

addExitProc proc =
  if conn != nil:
    conn.close

proc roundtrip*(msg: string): Future[string] {.async.} =
  assert conn != nil, "conn hasnt been init correctly"

  return await conn.roundtrip msg

proc roundtrip*(path, msg: string): Future[string] {.async.} =
  if conn == nil:
    conn = await path.newAsyncConnection

  return await conn.roundtrip msg
