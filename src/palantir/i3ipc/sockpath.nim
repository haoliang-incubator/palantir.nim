import std/[posix, posix_utils, os, strutils, strformat, osproc, logging]

proc issock(path: string): bool {.raises: [].} =
  var sr: Stat

  try:
    sr = path.stat
  except OSError:
    return false

  sr.st_mode.S_ISSOCK


proc findSockPath*: string =

  result = getEnv "I3SOCK"

  if result != "" and result.issock:
    logging.debug "i3ipc/sockpath found via env"
    return

  try:
    result = execProcess("i3", args = ["--get-socketpath"], options = {poUsePath})
    result.stripLineEnd
  except OSError:
    discard

  if result != "" and result.issock:
    logging.debug "i3ipc/sockpath found via bin"
    return

  block:
    let uid = getuid()
    let rundir = &"/run/user/{uid}/i3"

    for kind, path in rundir.walkDir:
      if kind != pcFile:
        continue
      if path.contains "ipc-socket.":
        result = path
        break

  if result != "" and result.issock:
    logging.debug "i3ipc/sockpath found via dir scan"
    return

  raise newException(OSError, "no i3 sock found")


proc main: int =
  echo "finally: ", findSockPath()


when isMainModule:
  quit main()
