import std/[net, ropes, json]

import ../sockpath
import ../protocol

proc showmem(ident: int) {.inline.}

proc main: int =
  let
    path = findSockPath()
    sock = newSocket(AF_UNIX, protocol = IPPROTO_IP)

  defer: sock.close()

  sock.connectUnix(path)

  sock.send(mGetWorkspaces.pack(""))

  let
    header = sock.recv(headerSpec.len)
    (_, payloadSize) = header.unpackHeader()

  var
    payload: Rope
    remain = payloadSize

  payload = new Rope

  while remain > 0:
    let chunk = sock.recv(size = remain)
    assert chunk != "", "socket was broken"
    payload.add(chunk)
    remain -= chunk.len

  let jn = parseJson($payload)

  echo jn

when defined(inspectmem):
  proc showmem(ident: int) {.inline.} =
    echo "#", ident, "used-mem: ", getOccupiedMem()
else:
  proc showmem(ident: int) {.inline.} =
    discard

when isMainModule:
  0.showmem
  discard main()
  1.showmem

  echo "ownedEnabled? ", defined(nimOwnedEnabled)


# asyncrun: nim c -r -d:inspectmem
