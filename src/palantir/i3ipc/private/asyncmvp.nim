import chronos, chronos/transport, chronos/streams/asyncstream
import std/[sequtils, strutils]

import ../sockpath, ../protocol

proc showmem(ident: int) {.inline.}

proc main: Future[int] {.async.} =
  showmem(1)
  let transport = block:
    let
      path = findSockPath()
      address = path.initTAddress
    await address.connect
  showmem(2)

  let
    reader = transport.newAsyncStreamReader
    writer = transport.newAsyncStreamWriter
  showmem(3)

  defer: await transport.closeWait
  defer: await writer.closeWait
  defer: await reader.closeWait

  await writer.write mRunCommand.pack("nop")
  # TODO@haoliang why writer.finish, but no reader.finish?
  # # TODO@haoliang why after called writer.finish, writer.finished is false?
  await writer.finish
  showmem(4)

  var rawHeader = newSeq[byte](headerSpec.len)
  await reader.readExactly(rawHeader[0].addr, headerSpec.len)
  let (_, payloadSize) = rawHeader.unpackHeader
  showmem(5)

  var payload = newSeq[byte](payloadSize)
  await reader.readExactly(payload[0].addr, payloadSize)
  showmem(6)

  echo payload
  echo payload.join
  echo payload.foldl(a & b.chr, "")
  showmem(7)


when defined(inspectmem):
  proc showmem(ident: int) {.inline.} =
    echo "#", ident, "used-mem: ", getOccupiedMem()
else:
  proc showmem(ident: int) {.inline.} =
    discard


when isMainModule:
  showmem(0)
  discard waitFor main()
  showmem(8)

# asyncrun: nim c -r --mm:orc -d:inspectmem
