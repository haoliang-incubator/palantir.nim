import std/[net, exitprocs]

type Connection* = object
  sock: Socket

proc init(self: ref Connection, sockpath: string) =
  ## init self.sock

proc close(self: ref Connection) =
  if self.sock != nil:
    self.sock.close

proc roundtrip(self: ref Connection, msg: string) : string =
  return msg

let conn = new(Connection)
addExitProc(proc() =
  echo "atexit, closing conn"
  conn.close
)

proc roundtrip(sockpath, msg: string) : string =
  if conn.sock == nil:
    conn.init sockpath

  return conn.roundtrip(msg)

proc main: int =
  echo roundtrip("", "hello and welcome")


when isMainModule:
  quit main()
