import std/[net, ropes, locks, exitprocs]

import ./protocol

type Connection* = object
  sock: Socket
  sendlock: Lock
  recvlock: Lock


using self: ref Connection

proc init(self; path: string) =
  let sock = newSocket(AF_UNIX, protocol = IPPROTO_IP)
  sock.connectUnix path
  self.sock = sock

  self.sendlock.initLock
  self.recvlock.initLock

proc newConnection*(path: string): ref Connection =
  new result
  result.init path

proc close*(self) =
  try:
    self.sock.close
  except:
    stderr.write "i3 sock close failed \n"

  try:
    self.sendlock.deinitLock
  except:
    stderr.write "sendlock deinit failed \n"

  try:
    self.recvlock.deinitLock
  except:
    stderr.write "recvlock deinit failed \n"


proc sendmsg(self; msg: string) =
  self.sock.send msg

proc recvmsg(self): string =
  let sock = self.sock

  let
    header = sock.recv headerSpec.len
    (_, payloadSize) = header.unpackHeader()

  var
    payload: Rope
    remain = payloadSize

  while remain > 0:
    let chunk = sock.recv(size = remain)
    assert chunk != "", "socket was broken"
    payload.add chunk
    remain -= chunk.len

  return $payload


proc roundtrip*(self; msg: string): string =
  withLock self.sendlock:
    self.sendmsg msg

  withLock self.recvlock:
    return self.recvmsg

var conn: ref Connection

addExitProc proc =
  if conn != nil:
    conn.close

proc roundtrip(msg: string): string =
  assert conn != nil, "conn hasnt been init correctly"
  conn.sendmsg msg
  conn.recvmsg

proc roundtrip*(path, msg: string): string =
  if conn == nil:
    conn = path.newConnection

  roundtrip msg
