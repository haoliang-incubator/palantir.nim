import std/[unittest]

import ../src/palantir/i3ipc/[connection, protocol, sockpath]


suite "connection":

  test "sockpath":
    let path = findSockPath()

    check:
      path != ""

  test "roundtrip":
    let
      path = findSockPath()
      conn = newConnection(path)

    defer: conn.close

    let reply = conn.roundtrip mRunCommand.pack("nop")

    check:
      reply == """[{"success":true}]"""
