import std/[unittest, strutils]

import ../src/palantir/i3ipc/protocol

suite "protocol":

  test "pack":
    # see https://i3wm.org/docs/ipc.html#_sending_messages_to_i3
    let
      msg = mRunCommand.pack("exit")

    check:
      msg.toHex() == "69332D697063040000000000000065786974"

  test "unpack":
    let
      msg = mRunCommand.pack("exit")
      header = msg[0 .. headerSpec.high]

    check:
      header.unpackHeader() == (mRunCommand, 4)
