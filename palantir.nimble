# Package

version = "0.1.0"
author = "haoliang"
description = "palantir"
license = "GPL-2.0"
srcDir = "src"
bin = @["palantir"]
binDir = "bin"


# Dependencies

requires "nim >= 1.6.2"
# requires "swayipc >= 3.1.8 & < 4.0.0"
requires "struct >= 0.2.3 & < 1.0.0"
requires "chronos >= 3.0.11 & < 4.0.0"
