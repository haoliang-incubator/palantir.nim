.PHONY: tests


tests:
	nim c -r --threads:on tests/tests.nim

clean:
	git clean -iX


i3-cmd:
	nim c -d:asyncimpl -o:bin/i3-cmd-async src/palantir/i3cmd.nim
	nim c --threads:on -o:bin/i3-cmd src/palantir/i3cmd.nim
